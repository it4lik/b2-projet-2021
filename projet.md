# Projet Infra

## Sommaire

- [Projet Infra](#projet-infra)
  - [Sommaire](#sommaire)
  - [Intro](#intro)
  - [Sujet](#sujet)
    - [Indicateurs de monitoring](#indicateurs-de-monitoring)
    - [Dossiers et bases de données à backup](#dossiers-et-bases-de-données-à-backup)
    - [Bonnes pratiques de sécurité](#bonnes-pratiques-de-sécurité)
  - [Dates](#dates)
  - [Notation](#notation)
    - [Notation continue](#notation-continue)
    - [Oral](#oral)
    - [Ecrit](#ecrit)

## Intro

Le projet infra vise à vous faire mettre en pratique vos compétences en système, infra, réseau et sécurité dans le but de répondre à un besoin réel.  
Vos compétences en développement peuvent aussi très bien être sollicitées.

Cela permet de concrétiser les compétences acquises pendant les premiers cours, en se rapprochant d'un cas plus tangible.

Le projet doit se faire en groupe de 2 à 4 personnes et dure ~2 mois.

## Sujet

**Le choix du sujet est libre**, vous devez vous-mêmes apporter votre sujet.

L'idée est la suivante :

- partez d'un vrai besoin, de quelque chose dont vous auriez besoin, dont vous seriez fiers
- discussion autour de la faisabilité du projet
- réalisation du projet, en groupe

> Il sera strictement nécessaire de bosser entre les séances allouées au soutien projet pour rendre quelque chose de correct.

Peu importe le sujet choisi, quelques bonnes pratiques :

- documentation des installations réalisées
- monitoring
  - si une vraie solution de monitoring n'est pas en place, il faut au moins remonter [les indicateurs de monitoring](#indicateurs-de-monitoring)
- backup
  - si une vraie solution de backup n'est pas en place, il faut au moins remonter [les dossiers/base de données que vous auriez backup](#dossiers-et-bases-de-données-à-backup)
- respect des bonnes pratiques élémentaires de sécurité
  - firewall, gestion des utilisateurs, hygiène d'administration, etc

### Indicateurs de monitoring

On désigne par ce terme la liste des choses à surveiller, pour l'application de monitoring. Par exemple :

- surveillance de la RAM
- surveillance des cartes réseau
- surveillance de la disponibilité d'un site web
- surveillance de la disponibilité d'une base de données

Ces indicateurs, cette liste, devra figurer dans le rendu écrit (dépôt git) sous forme de tableau par exemple.

### Dossiers et bases de données à backup

Vous devrez faire figurer la liste des dossiers et base de données à backup, en précisant sur quelle machine il/elle se trouve.

On trouvera notamment...

- les dossiers qui contiennent les fichiers de conf liées à votre application
- les dossiers qui contiennent des données liés à votre application

Cette liste figurera dans le compte-rendu écrit (dépôt git).

## Dates

Le projet Infra YNOV s'étendra sur un peu moins de 2 mois :

| Séance   | Date  |
|----------|-------|
| Séance 1 | 05/04 |
| Séance 2 | 19/04 |
| Séance 3 | 22/04 |
| Séance 4 | 06/05 |
| Séance 5 | 16/05 |
| Séance 6 | 19/05 |
| Séance 7 | 24/05 |

**La séance 7 est le jour de l'oral.**
