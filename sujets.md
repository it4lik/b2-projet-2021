| Membres                                | Sujets                                                               | Git |
|----------------------------------------|----------------------------------------------------------------------|-----|
| Loisel, Michel                         | p/f streaming video, hébergé dans k8s, dans un VPS                   |     |
| Montagnier, Peyrataud                  | azure cloud, containerized app                                       |     |
| Queysnoy, Farant                       |                                                                      |     |
| Fallous, Flambard, Cloux               | file storage, client backup                                          |     |
| Garcia, Fonseca                        | porter une webapp, ct, self-hosted                                   |     |
| Dautrement, Granger, Reype             | webapp, auto hébergée, stockage                                      |     |
| Mourgues, Ferreira, Heraud             | webapp, ci/cd                                                        |     |
| Marechal, Adonai                       | self hosted multiplayer webapp tictactoe                             |     |
| Roux, Vigneron, Manant                 | porter webapp, vraie infra                                           |     |
| Martinez                               | self hosted multiplayer webapp tictactoe                             |     |
| Abeille, Coco, Bourrin, Gautier        | self hosted multiplayer action game                                  |     |
| Levée, Curmi, Tarandeau                | UNO unity, matchmaking online                                        |     |
| Ingremeau, Sery, Flamant               | Flight simulator, db api contexte                                    |     |
| Roussat, Boyer, Augier-de-Moussac      | voir antoine thys, infra qui porte service, infra qui permet le dév  |     |
| Bertin, Perrin, Marchand, Muraleedaran | dames C#, client lourd, matchmaking                                  |     |
| Diologent, Debengy                     | crypto/data : gestion de db, hébergement db + web, blockchain locale |     |
| Merlaud, Jouannet, Milh                | Dockerize apps                                                       |     |
| Gelineau, Joyet, Clamadieu             | chess, self hosted, web n-tiers (ci ? docker ?)                      |     |
| Megy, De Freitas, Aumeunier, Kolo      | git                                                                  |     |
| Gregoire, Chasson                      | sécu, hack then fix                                                  |
